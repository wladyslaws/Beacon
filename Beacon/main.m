//
//  main.m
//  Beacon
//
//  Created by WLADYSLAW SURALA on 30/12/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
