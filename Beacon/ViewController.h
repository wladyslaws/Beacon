//
//  ViewController.h
//  Beacon
//
//  Created by WLADYSLAW SURALA on 30/12/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BeaconAdvertisementUtil.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController : NSViewController<CBPeripheralManagerDelegate>

@property (nonatomic,strong) CBPeripheralManager *manager;

@property (nonatomic, strong) BeaconAdvertisementUtil *utility;

@end

