//
//  ViewController.m
//  Beacon
//
//  Created by WLADYSLAW SURALA on 30/12/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager = [[CBPeripheralManager alloc] initWithDelegate:self
                                                       queue:nil];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        
        NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:@"yourUUIDhere"];
        
        self.utility = [[BeaconAdvertisementUtil alloc] initWithProximityUUID:proximityUUID
                                                                                               major:1
                                                                                               minor:1
                                                                                       measuredPower:-59];
        
        
        [_manager startAdvertising:self.utility.beaconAdvertisement];
    }
}

@end
