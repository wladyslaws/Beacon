//
//  AppDelegate.m
//  Beacon
//
//  Created by WLADYSLAW SURALA on 30/12/16.
//  Copyright © 2016 WLADYSLAW SURALA. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
